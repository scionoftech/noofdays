[![noofdays Logo](https://github.com/scionoftech/noofdays/blob/Development/images/nlogo.png)](https://www.npmjs.com/package/noofdays)

[noofdays v1.1.0](https://www.npmjs.com/package/noofdays)

  A small library for getting **no of days** between two dates for [node](http://nodejs.org).

  ```js
//import noofdays
var days=require("noofdays");

//give start_date and end_date
var number=days.noofdays("start_date","end_date");

//start_date=09/08/2015 and end_date=16/08/2015

//number=7
console.log(number);
```

## Installation

```bash
$ npm install noofdays
```

## License

  [ISC](LICENSE)
