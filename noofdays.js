
module.exports.noofdays=function(startdate,enddate)
{
	//startdate and edndate will be in this format dd/mm/yyyy
	var stdate=startdate;
	var endate=enddate;
	
	var sdate = stdate.split('/');//split string to get date,month and year
	var edate=endate.split('/');

	var oneDay = 24*60*60*1000;	// hours*minutes*seconds*milliseconds
	var firstDate = new Date(sdate[2],sdate[1],sdate[0]);
	var secondDate = new Date(edate[2],edate[1],edate[0]);

	var diffDays = Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay));//Math.abs wil return the absolute value of a number

	return diffDays;
}